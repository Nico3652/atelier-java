package characters;

import lsg.helpers.Dice.Dice;
import lsg.weapons.Sword.Sword;

public class Monster extends Characters {
	
	
	public Monster() {
		
		INSTANCES_COUNT += 1;

		name = "Monster_" + INSTANCES_COUNT;
		
		this.life = 50;
		this.stamina = 150;
		
	}
	
	
	
	public Monster(String monsterName) {
		
		name = monsterName;
		
		this.life = 10;
		this.stamina = 10;
	}
	
	
	public static int INSTANCES_COUNT = 0;
	private Dice monsterDice;
	private Sword monsterSword;
	
	
	
	private void init() {
		isAlive();
		printStats();
		monsterSword = new Sword();
		monsterSword.printStats();
	}
	
	
	

}
