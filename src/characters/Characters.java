package characters;

import lsg.helpers.Dice.Dice;
import lsg.weapons.Weapon.Weapon;

public class Characters {
	
	public Characters() {
		
	}
	
	public String name;
	public int life = 100;
	public int maxLife;
	public int stamina = 50;
	public int maxStamina;
	public String alive;
	public Weapon weaponChar;
	
	
	
	
	public void setName(String charName) {
		name = charName;
	}

	public String getName() {
		return name;
	}
	
	
	public void setLife(int charLife) {
		life = charLife;
	}
	
	public int getLife() {
		return life;
	}
	
	public void setMaxLife(int charMaxLife) {
		maxLife = charMaxLife;
	}
	
	public int getMaxLife() {
		return maxLife;
	}
	
	public void setStamina(int charStamina) {
		stamina = charStamina;
	}
	
	public int getStamina() {
		return stamina;
	}
	
	
	public void setMaxStamina(int charMaxStamina) {
		maxStamina = charMaxStamina;
	}
	
	public int getMaxStamina() {
		return maxStamina;
	}
	
	
	// Check if the characters is Alive or not
	// If isAlive return true, false otherwise
	public Boolean isAlive() {
		if(life > 0) {
			alive = "(ALIVE)";
			return true;
		}
		else {
			alive = "(DEAD)";
			return false;
		}
	}
	
	
	// Members are placed inside "stats" and returned to be printed in printStats()
	public String toString() {
		
		//String format = String.format("%-20s");
		String stats = "[ " + getClass().getSimpleName() + " ]" 
					   + "\t" + name 
					   + "\t LIFE:" + life 
					   + " \t STAMINA:" + stamina
					   + "\t" + alive;
		return stats;
	}
	
	
	// Display characters stats
	public void printStats() {
		System.out.println((toString()));
	}
	
	

	
	public void setWeapon(Weapon weapon) {
	
		weaponChar = weapon;
	}
	
	private Weapon getWeapon() {
		
		
		return weaponChar;
	}

	
	
	
	/**
	 * Handle differents cases :
	 * - if the weapon is broken
	 * - according the dice result
	 * - according weapon and hero'stamina 
	 * return the hitdamage value 
	 * @param weapon   the weapon used for the attack
	 * @return
	 */
	public int attackWith(Weapon weapon) {
		
		int lastDamage = 0;
		
		if(weapon.isBroken()) {
		//	printAttack(weapon, lastDamage);
			
			lastDamage = 0;

			return lastDamage;
		}
		else {
			
		
			Dice dice = new Dice(101);
			int diceResult = dice.roll();
			int minDamageOnDiceResult = (weapon.minDamage * diceResult / 100) + weapon.minDamage;
		
			weapon.durability -= 1;
			
			
			if(stamina > 0) {
				
				if (stamina - weapon.stamCost <= 0) {
					stamina = 0;
				}
				else {
					stamina -= weapon.stamCost;
				}			
				System.out.println("pass");
				
				
				
			}
			else {
				
				stamina  = 0;
				lastDamage = 0;
				System.out.println("pass2");
				
			//	printAttack(weapon, lastDamage);
			
				return lastDamage;
				
			}
			
			if(minDamageOnDiceResult > weapon.maxDamage) {
			//	printAttack(weapon, weapon.maxDamage);
				System.out.println("pass3");
				
		
				
				return weapon.maxDamage;
			}
			
			else {
				
				if(stamina < weapon.stamCost && stamina >= 0) {
					
					
					
					int staminaOnHundred = Math.round(stamina * 100 / weapon.stamCost);
					
				    lastDamage = Math.round((minDamageOnDiceResult) * staminaOnHundred / 100);
				  
				
				    
				    System.out.println("pass4");
				   // printAttack(weapon, lastDamage);
				    return lastDamage;
				}
				
				System.out.println("pass5");
				//printAttack(weapon, minDamageOnDiceResult);
			
				return minDamageOnDiceResult;
			}
		}
	}
	
	
    public int attack() {
		
    	if(getWeapon() != null) {
    	
    		return attackWith(getWeapon());
 
    	}
    	else {
    		
    		return 0;
    	}
    	

	}

    
    
    
    int pv = 0;
	
	public int getHitWith(int value) {
		
		//pv = (value < life) ? (life-value) : 0;
		
		System.out.println("pv" + pv);
		
		
		life = life - value;
		
		printResult(value);
		
		return life;
	}
	
	
	private void printResult(int hit) {
		
		String pvResult = "!!! " 
				+ name 
				+ " attack " 
				+ name 
				+ " with " 
				+ weaponChar.name 
				+ " ("
				+ hit
				+ ") !!! -> Effective DMG : "
				+ life
				+ " PV";
		
		
		
		System.out.println("");
		System.out.println(pvResult);
		System.out.println("");
		
	}
	
	
	private void printAttack(Weapon weapon, int lastDamage) {
		
		String attack = "attack with " + weapon.toString() + " > " + lastDamage;
		
		String pvResult = "!!! " 
					+ name 
					+ " attack " 
					+ name 
					+ " with " 
					+ weapon.name 
					+ " ("
					+ lastDamage
					+ ") !!! -> Effective DMG : "
					+ life
					+ " PV";
	
		//System.out.println(attack);
		System.out.println("");
		System.out.println(pvResult);
		System.out.println("");
	}
	
	
	
	
	

}
