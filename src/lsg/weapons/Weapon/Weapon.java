package lsg.weapons.Weapon;

public class Weapon {

	
	public Weapon() {
	}
	
	public Weapon(String wName, int wMinDamage, int wMaxDamage, int wStamCost, int wDurability){
		this.name       = wName;
		this.minDamage  = wMinDamage;
		this.maxDamage  = wMaxDamage;
		this.stamCost   = wStamCost;
		this.durability = wDurability;
	}
	
	
	public String name;
	public int minDamage;
	public int maxDamage;
	public int stamCost;
	public int durability;
	
	
	public String getName() {
		return name;
	}
	
	public int getMinDamage() {
		return minDamage;
	}
	
	public int getMaxDamage() {
		return maxDamage;
	}
	
	public int getStamCost() {
		return stamCost;
	}
	
	public int getDurability() {
		return durability;
	}
	
	private int setDurability() {
		return durability;
	}
	
	
	public int use() {
		
		// In case where the weapon would be broken
		if(durability > 0) {
			durability = durability - 1; 
		}
		
		return durability;
	}
	
	
	public boolean isBroken() {
		
		if(durability == 0) {
			return true;
		}
		else {
			return false;
		}
	}
	
	
	
	
	public String toString() {
		String stats = name + " (min:" + minDamage + " max:" + maxDamage + " stam:" + stamCost + " dur:" + durability + ")";
		
		return stats;
	}
	

	public void printStats() {
		System.out.println(toString());
	}
	
	
	
}
