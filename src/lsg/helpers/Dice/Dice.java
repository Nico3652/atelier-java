package lsg.helpers.Dice;

import java.util.Random;

public class Dice {
	

	public Dice() {
	}
	
	public Dice(int diceFaces) {
		this.faces = diceFaces;	
	}
	
	
	int faces;
	Random random;
	
	public int roll() {
		random = new Random();
		int number = random.nextInt(faces);
		
		return number;
	}
	
	
	
}
