package lsg;

import java.util.Scanner;

import characters.Hero;
import characters.Monster;
import lsg.helpers.Dice.Dice;
import lsg.weapons.Claw.Claw;
import lsg.weapons.ShotGun.ShotGun;
import lsg.weapons.Sword.Sword;
import lsg.weapons.Weapon.Weapon;


/*
 * Partie 3 Q 1.2 :
 * Il faudra passer par private ou protected pour empecher l'accès depuis certaines
 * classes ou certains packages
 * 
 * Q 6 Classe LearningSoul :
 * La durabilité de l'épée descend en double pour le hero et pour le monstre
 * 
 */


public class LearningSoulsGame {
	
	
	private static Hero mainHero;
	private static Monster monster;
	private static Scanner scanner;
	
	
	public static void main(String[] args) {
		
		

		fight1v1();

		
	}
	
	
	private static void init() {
		mainHero = new Hero();
		monster = new Monster();
		
		mainHero.setWeapon(new ShotGun());
		monster.setWeapon(new Claw());
		
		refresh();
		
	}
	
	
	private static void refresh() {
		
		mainHero.printStats();
        monster.printStats();
        mainHero.isAlive();
        monster.isAlive();
		
	}
	
	 private static void fight1v1(){
		 
		 init();
		
	        
        while(mainHero.isAlive() && monster.isAlive()){
     
        	
        	refresh();
            
            int hitValue = mainHero.attack(); 
            int damage = monster.getHitWith(hitValue);
            
            refresh();
            
            int monsterHit = monster.attack();
            int monsterDamage = mainHero.getHitWith(monsterHit);
            
            
        }
        
        
        refresh();
        
	 }
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 

}
